import { LitElement, html } from 'lit-element';

class AccountsFormPage extends LitElement {
    
    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <form>
                <div class="form-group">
                    <label >Nombres y apellidos</label>
                    <input type="text" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                    <label >Email address</label>
                    <input type="email" class="form-control" id="" placeholder="name@example.com">
                </div>
                <div class="form-group">
                    <label >Cuidad</label>
                    <input type="text" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                    <label >Distrito</label>
                    <input type="text" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                    <label >Direccion</label>
                    <input type="text" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Seleccione la cuenta que desee</label>
                    <select class="form-control" id="">
                    <option>Cuenta en Dolares</option>
                    <option>Cuenta en Soles</option>
                    </select>
                </div>
                <button @click="${this.goBack}" class="btn btn-primary">
                        <strong>Atras</strong>
                    </button>
                    <button @click="${this.saveAccount}" class="btn btn-success">
                        <strong>Enviar</strong>
                    </button>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
            </form>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("accounts-form-back",{}));
    }

    saveAccount(e) {
        console.log("saveAccount");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent('accounts-form-save', {
            detail: {
                id: "5fd281e0bed09d1370d8545",
                nroCuenta: "1234-5678-910111444444",
                producto: {
                    tipo: "PASIVO",
                    nombre: "Cuenta Dolares Free"
                },
                codigoCliente: "C0001",
                saldoDisponible: 0.0,
                saldoConsumido: 0.0,
                moneda: "USD",
                activo: true
            }
        }));
    }

}

customElements.define('accounts-form-page', AccountsFormPage);
