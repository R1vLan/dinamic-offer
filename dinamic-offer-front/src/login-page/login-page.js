import { LitElement, html, css } from 'lit-element';
import '../header-page/header-page.js';
import '../footer-page/footer-page.js';

//service
import { serviceLogin } from '../services/services.js';

class LoginPage extends LitElement {
    
    static get styles() {
        return css`
          #login-content {
            display: block;
            color:white;
          }

          .outer {
            display: table;
            position: absolute;
            background: rgb(29, 115, 178);
            top: 128px;
            left: 0px;
            height: 76%;
            width: 100%;
           }

          .middle {
            display: table-cell;
            vertical-align: middle;
          }

          .inner {
            margin-left: auto;
            margin-right: auto;
            margin-top: -2rem;
          }

          .spaceCheck {
            padding-top: 11px;
          }

          .buttonStyle {
            margin-left: auto;
            margin-right: auto;
            padding: 0.8rem 3rem;
            margin-top: 0.9rem;
            width: 100%;
          }

          #idError {
            color: #e08585;
            font-size: 17px;
          }
        `;
    }

    static get properties() {
        return {
            userData: {type: Object},
            typeDocument: {type: String},
            numberDocument: {type: String},
            password: {type: String},
            messageError: {type: String}
        };
    }

    constructor() {
        super();
        this.userData = [];
        this.typeDocument = 'L';
        this.numberDocument = '';
        this.password = '';
        this.messageError = '';
    }
    
    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">            
            <main>
                <div id="login-content">
                    <div class="outer">
                        <div class="middle">
                            <div class="inner">
                                <div class="col-10 mx-auto">
                                <h3>¡Bienvenido!</h3>
                                <br/>

                                <select class="form-control form-control-lg" @input="${this.updateTypeDocument}">
                                    <option value="L" selected>DNI</option>
                                    <option value="R">RUC</option>
                                    <option value="P">Pasaporte</option>
                                    <option value="E">Carnet de Extranjería</option>
                                    <option value="M">Carnet Identidad Militar</option>
                                    <option value="D">Carnet Diplomático</option>
                                    <option value="J">Partida de Nacimiento</option>
                                </select>
                                <br/>

                                <input 
                                    class="form-control form-control-lg" 
                                    type="number" 
                                    placeholder="Número de documento"
                                    @input="${this.updateNumberDocument}" 
                                    .value="${this.numberDocument}"
                                    />

                                <div class="form-check mb-2 spaceCheck">
                                    <input class="form-check-input" type="checkbox" id="autoSizingCheck">
                                    <label class="form-check-label" for="autoSizingCheck">
                                    Recordar documento
                                    </label>
                                </div>
                                <br/>

                                <input 
                                    class="form-control form-control-lg" 
                                    type="password" 
                                    @input="${this.updatePassword}" 
                                    .value="${this.password}"
                                    placeholder="Contraseña de Banca por Internet"
                                    />
                                <br/>

                                <div id="idError">${this.messageError}</div>
                            
                                
                                    <button class="btn btn-primary buttonStyle" type="button" @click="${this.goLogin}">Ingresar</button>
                                
                                <br/>

                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </main>

        `;
    }

 
    updateTypeDocument(e) {
        console.log("updateNumberDocument " + e.target.value);
        this.typeDocument = e.target.value;
        this.messageError = '';
    }

    updateNumberDocument(e) {
        console.log("updateNumberDocument " + e.target.value);
        this.numberDocument = e.target.value;
        this.messageError = '';
    }

    updatePassword(e) {
        console.log("updatePassword " + e.target.value);
        this.password = e.target.value;
        this.messageError = '';
    }


    goLogin() {
        serviceLogin(this.typeDocument, this.numberDocument, this.password).then((data) => {
            if(data.error != undefined) {
                this.messageError = data.error;
    
            } else {
                console.log('success '+ data);
                this.userData = data;
                this.eventLogin();
            }
        }, (error) => {
            console.log("error " + error);
            this.messageError = error;
        });
    }

    eventLogin() {
        this.dispatchEvent(new CustomEvent("login-user-success", {
            detail: {
                userData: this.userData
            }
        }));
    }

}
customElements.define('login-page', LoginPage)