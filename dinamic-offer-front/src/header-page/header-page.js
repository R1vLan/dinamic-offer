import { LitElement, html, css } from 'lit-element';

class HeaderPage extends LitElement {
    
    static get styles() {
        return css`

          header {
            background: rgb(7, 33, 70);
            width: 100%;
            top: 0px;
            position: absolute;
          }

          img {
            margin-left: auto;
            margin-right: auto;
            display: block;
            width: 8rem;
            height: 8rem;
          }

        `;
    }

    static get properties() {
        return {
            
        };
    }

    constructor() {
        super();
    
    }
    
    render() {
        return html `
            <header>
                <div>
                    <img src="./src/img/logo-bbva.svg" alt="Logo" />
                </div>
            </header>
        `;
    }

}
customElements.define('header-page', HeaderPage)