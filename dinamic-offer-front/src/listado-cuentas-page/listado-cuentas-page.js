import { LitElement, html, css } from 'lit-element';

class ListadoCuentasPage extends LitElement {

    static get styles() {
        return css`
            .styleButton {
                margin-left: 2rem;
            }

            .titleProduct {
                margin-left: 3rem;
                margin-right: 3rem;
            }

            .contentData {
                display: flex;
                background:#f8f9f9;
                padding: 1rem;
            }

            .nroCuentClass {
                width:60%;
            }

            .fontNumberAccount {
                color: #4b74d4;
            }

            .saldoDisponibleClass {
                width:20%;
                text-align: center;
                padding-right: 17px;
            }

        `;
    }

   
    static get properties() {
        return {
            nroCuenta: {
                type: String
            },
            tipoCuenta: {
                type: String
            },
            saldoDisponible: {
                type: Number
            }
        }
    }

    constructor(){
        super();
    }

    render() {
        return html `
            <!--Boot strap importarlo-->
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            
            <div class="titleProduct">
                    <div class="contentData">
                        <div class="nroCuentClass">
                            <span class="fontNumberAccount">${this.nroCuenta}</span><br>
                                ${this.tipoCuenta}
                            </div>

                            <div class="saldoDisponibleClass">${this.saldoDisponible.toFixed(2)}</div>
                            
                            <button @click="${this.listMov}" class="btn btn-info col-sm-1 styleButton"><strong>Ver</strong></button>
                        </div>
                    </div>
        `;
    }

    listMov() {
        console.log("listar movimientos");
        this.dispatchEvent(new CustomEvent("listar-mov", {
            detail: {
                nroCuenta: this.nroCuenta
            }
        }))
    }

}

customElements.define('listado-cuentas-page', ListadoCuentasPage);