import { urlLogin } from '../utils/constants-utils.js';

export const serviceLogin = async function(_typeDocument, _document, _password) {
    console.log("serviceLogin " + _typeDocument + _document + " clave: "+ _password);
    try {
        let initObject = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(
                {
                    "username": _document, //'12345678', 
                    "password": _password //'hackathon'
                }
            )
        };

        let result = await fetch(urlLogin, initObject);
        return result.json();
    
    } catch (error) {
        throw error;
    }
}