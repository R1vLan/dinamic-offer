import { LitElement, html } from 'lit-element';

class ButtonsLeftPage extends LitElement {
    render() { //parte visual del web component
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
                <!--
                <div class="border border-primary bg-dark">
                    <div style="text-align: center;">
                    <p class="text-center text-white font-weight-bold" >BBVA T-CAMBIO</p>
                    </div>
                    <p class="text-center text-white" > Aqui encontraras un tipo de cambio preferencial para ti</p>
                    <div class="text-center">                    
                        <button @click="${this.inProgress}" class="btn btn-info btn-sm" style="width:200px;"><strong>T-Cambio</strong></button><br>
                    </div>
                </div>
                <br>
                -->
                <div>         
                    <button @click="${this.inProgress}" class="btn btn-outline-primary w-25"><strong>Abre una nueva cuenta</strong></button>
                </div>
                <div>         
                    <button @click="${this.inProgress}" class="btn btn-outline-primary w-25"><strong>Operaciones frecuentes</strong></button>
                </div>
                <br>
                <div>                
                    <button @click="${this.inProgress}" class="btn btn-outline-primary w-25"><strong>Paga tus servicios</strong></button>
                </div>
                <br>
                <div>                
                    <button @click="${this.inProgress}" class="btn btn-outline-primary w-25"><strong>Pago de tarjeta</strong></button>
                </div>
                <br>
                <div>                
                    <button @click="${this.inProgress}" class="btn btn-outline-primary w-25"><strong>Programa tus beneficios</strong></button>
                </div>
                <br>
                <div>                
                    <button @click="${this.inProgress}" class="btn btn-outline-primary w-25"><strong>Mi salud financiera</strong></button>
                </div>
        `;
    }

    inProgress(e) {
        console.log("inProgress");
        this.dispatchEvent(new CustomEvent("in-progress", {}));
    }
}

customElements.define('buttons-left-page', ButtonsLeftPage);