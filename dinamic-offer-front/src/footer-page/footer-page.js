import { LitElement, html, css } from 'lit-element';

class FooterPage extends LitElement {
    
    static get styles() {
        return css`
          #footer-content {
            background: rgb(7, 33, 70);
            display: flex;
            position: fixed;
            bottom: -27px;
            width: 100%;
            padding-top: 1rem;
            padding-bottom: 1rem;
            height: 4.5rem;
          }

          div {
            padding: 1rem;
            color:white;
            margin-left: auto;
            margin-right: auto;
            display: block;
            text-align: center;
          }
        `;
    }

    static get properties() {
        return { 
        };
    }

    constructor() {
        super();
    }
    
    render() {
        return html `
            <footer id="footer-content">
                <div>
                    TechU Practitioner - @Copyright 2020
                    <br>
                    Equipo 3  [ Alan Rivera, Erick Agüero, Gustavo Ramos, Julio Cayulla ]
                </div>
            </footer>
        `;
    }

}
customElements.define('footer-page', FooterPage)