import { LitElement, html } from 'lit-element';
import '../login-page/login-page.js';
import '../offer-main/offer-main.js';

class OfferApp extends LitElement {
    
    static get properties() {
        return {
            isLoginGoHome: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.userData = [];
        this.isLoginGoHome = false;
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <header-page></header-page>

            <login-page id="login" @login-user-success="${this.goHome}"></login-page>
            <offer-main id="offer-main" class="d-none border rounded border-primary"></offer-main>
            <!--<offer-main id="homeOffer"></offer-main>-->
            <footer-page></footer-page>

        `;
    }


    goHome(e) {
        console.log("validationLogin " + JSON.stringify(e.detail.userData) + " go Home");
        console.log("Codigo cliente: " + e.detail.userData.codigoCliente);
        this.shadowRoot.getElementById("offer-main").codCliente = e.detail.userData.codigoCliente;
        this.shadowRoot.getElementById("offer-main").userName = e.detail.userData.nombre + ' '+ e.detail.userData.apellido;
        this.shadowRoot.getElementById("offer-main").classList.remove("d-none");
        this.shadowRoot.getElementById("login").classList.add("d-none");
    }


}

customElements.define('offer-app', OfferApp);