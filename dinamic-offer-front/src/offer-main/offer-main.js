import { LitElement, html, css } from 'lit-element';
import '../listado-cuentas-page/listado-cuentas-page.js';

class OfferMain extends LitElement {

    static get styles() {
        return css`
            .home-content {
                display: table;
                position: absolute;
                top: 130px;
                left: 0px;
                height: 76%;
                width: 100%;
                margin-top: 13px;
            }

            .spaceContent {
                margin-bottom: 5rem;
                margin-left: 1px;
                flex: auto;
            }

            .titleProduct {
                margin-left: 3rem;
                margin-right: 3rem;
            }

            .contentTitle {
                display: flex;
                background: white;
                padding: 1rem;
                font-weight: bold;
            }
            .userNameClass {
                width: 39%;
                margin-left: 3rem;
                margin-right: 3rem;
                color: white;
                background: rgb(76 98 125);
                padding: 1rem;
                font-size: 1.3rem;
                margin-bottom: 1rem;
            }

            .welcomeProduct {
                margin-left: 3rem;
                margin-right: 3rem;
                color: rgb(76, 98, 125);
                padding: 1rem;
                margin-bottom: 1rem;
                font-weight: bold;
                font-size: 1.3rem;
                text-align: left;
            }

            .contentOffer {
                margin-left: 3rem;
                margin-right: 3rem;
            }
        `;
    }

    static get properties() {
        return {
            accounts: {
                type: Array
            },
            offers: {
                type: Array
            },
            codCliente: {
                type: String
            },
            userName: {
                type: String
            },
            messageWelcome: {
                type: String
            }
        }
    }

    constructor() {
        super();
        this.accounts = [];
        this.userName = '';
        this.offers = [];
        this.messageWelcome = '';
    }

    render() { 
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <div class="home-content">
                <div id="listAccounts">
                <div class="userNameClass">Hola, ${this.userName}!!</div>
                <main>
                    <div class="titleProduct">
                    <div class="contentTitle">
                            <div style="width:60%;">Numero y tipo de cuenta</div>
                            <div style="width:20%;">Saldo Disponible</div>
                            <div style="width:20%;">Ver movimientos</div>
                        </div>
                    </div>

                    <div class="contentProduct">
                        ${this.accounts.map(
                            counts => html `<listado-cuentas-page
                            nroCuenta="${counts.nroCuenta}"
                            tipoCuenta="${counts.producto.nombre}"
                            saldoDisponible="${counts.saldoDisponible}">
                            </listado-cuentas-page>`
                        )}
                    </div>
                </div>
                </main>
            <br/>
            <div id="botonAbrir">
            <button @click="${this.formAccount}" class="btn btn-primary text-right text-right" style="margin-left: 45px;"><strong>Abre una nueva cuenta</strong></button>
            </div>
            <br/>
            
            <main>
                <div class="welcomeProduct">${this.messageWelcome}</div>
                <div class="row text-center contentOffer" id="listOffers">
                    <div class="row row-cols-1 row-cols-sm-3 spaceContent">
                        ${this.offers.map(
                            allOffers => html `<cards-offer-page style="margin: 0 auto;"
                                nombreOffer="${allOffers.nombre}" 
                                tituloOffer="${allOffers.titulo}"
                                descripcionOffer="${allOffers.descripcion}"
                                urlBanner="${allOffers.urlBanner}"
                                landingUrl="${allOffers.landingUrl}"
                                @open-landing="${this.openLanding}"> 
                            </cards-offer-page>`
                        )}
                    </div>
                </div>
            </main>
            <br>
            <br>
            <accounts-form-page id="formAccounts" class="d-none border rounded border-primary"
                @accounts-form-back="${this.backHome}"
                @accounts-form-save="${this.saveAccount}"
            ></accounts-form-page>
            </div>
        `;
    }

    formAccount() {
        console.log("newAccount");
        this.shadowRoot.getElementById("listAccounts").classList.add("d-none");
        this.shadowRoot.getElementById("listOffers").classList.add("d-none");
        this.shadowRoot.getElementById("formAccounts").classList.remove("d-none");
        this.shadowRoot.getElementById("botonAbrir").classList.add("d-none");
        
        this.messageWelcome = "";
        

    }

    backHome() {
        console.log("backHome");
        this.shadowRoot.getElementById("listAccounts").classList.remove("d-none");
        this.shadowRoot.getElementById("listOffers").classList.remove("d-none");
        this.shadowRoot.getElementById("formAccounts").classList.add("d-none");
        this.shadowRoot.getElementById("botonAbrir").classList.remove("d-none");
        if(this.offers.length > 0) {
            this.messageWelcome = "¡Por qué sabemos que es lo que necesitas, te recomendamos los mejor!";
        }
    }

    saveAccount(e) {
        console.log("saveAaccount");
        console.log(JSON.stringify(e.detail));
        this.shadowRoot.getElementById("listAccounts").classList.remove("d-none");
        this.shadowRoot.getElementById("listOffers").classList.remove("d-none");
        this.shadowRoot.getElementById("formAccounts").classList.add("d-none");
        this.shadowRoot.getElementById("botonAbrir").classList.remove("d-none");
        this.insertNewAccount(e.detail);
    }

    updated(changedProperties) {
        if(changedProperties.has("accounts")) {
            console.log("cuentas updated");
            this.validateProducts();
        }
        if(changedProperties.has("codCliente")) {
            console.log("Se ha encontrado codigo cliente en Offer-main");
            this.getAccounts();
        }
        if(changedProperties.has("offers")){
            console.log("Se han encontrado ofertas");
        }
    }

    validateProducts() {
        let cuentaSinSaldo = this.accounts.findIndex(
            sinSaldo => sinSaldo.saldoDisponible === 0
        );
        let cuentasDolares = this.accounts.findIndex(
            cuentasDolares => cuentasDolares.producto.nombre === 'Cuenta Dolares Free'
        );

        if(cuentaSinSaldo >= 0) {
            //console.log("Cuenta sin saldo: " + JSON.stringify(this.accounts[cuentaSinSaldo]));
            console.log("Numero de cuenta sin saldo: " + this.accounts[cuentaSinSaldo].nroCuenta);
        }

        if (cuentasDolares < 0) {
            console.log("La cuenta no tiene dolares");
        }
    }

    getAccounts() {
        console.log("getAccounts");
        let xhr = new XMLHttpRequest();
        let urlConsume = "http://ec2-44-235-21-6.us-west-2.compute.amazonaws.com:8081/accounts/"+this.codCliente;
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Obtencion de Accounts correcta");
                let APIResponse = JSON.parse(xhr.responseText);
                this.accounts = APIResponse;
                //console.log(JSON.stringify(this.accounts));
                if (this.accounts.length > 0) {
                    console.log("Se tiene más de 1 cuenta registrada");
                    this.getOffers();
                }
            }
        }
        xhr.open("GET", urlConsume);
        xhr.send();
    }

    getOffers() {
        console.log("getOffers");
        let xhr = new XMLHttpRequest();
        let json = JSON.stringify(this.accounts);
        let urlConsume = "http://ec2-44-238-219-22.us-west-2.compute.amazonaws.com:8082/customers/" + this.codCliente +"/offers";
        xhr.onload = () => {
        if (xhr.status === 200) {
            console.log("Petición Offers correcta");
            let APIResponse = JSON.parse(xhr.responseText);
            console.log(JSON.stringify(APIResponse));
            this.offers = APIResponse;
            if(this.offers.length > 0) {
                this.messageWelcome = "¡Por qué sabemos que es lo que necesitas, te recomendamos los mejor!";
            }
        }
        }
        xhr.open("POST", urlConsume);
        xhr.setRequestHeader("Content-Type", "application/json")
        xhr.send(json);
    }

    insertNewAccount(payload) {
        console.log("getOffers");
        let xhr = new XMLHttpRequest();
        //let json = JSON.stringify(this.accounts);
        let urlConsume = "http://ec2-44-235-21-6.us-west-2.compute.amazonaws.com:8081/accounts/";
        xhr.onload = () => {
        if (xhr.status === 200) {
            console.log("Petición creacion de cuenta correcta");
            let APIResponse = JSON.parse(xhr.responseText);
            //console.log(JSON.stringify(APIResponse));
            this.getAccounts();
        }
        }
        xhr.open("POST", urlConsume);
        xhr.setRequestHeader("Content-Type", "application/json")
        xhr.send(JSON.stringify(payload));
    }

    openLanding() {
        console.log("Se abrio una landing en offer-main");
        let xhr = new XMLHttpRequest();
        let json = JSON.stringify({
            offerCode:"OF001",
            codigoCliente:"C0002",
            abierto:true,
            fecha:"11-12-2020"
        });
        xhr.onload = () => {
        if (xhr.status === 200) {
            console.log("Petición landingPage correcta");
            //let APIResponse = JSON.parse(xhr.responseText);
            //console.log(JSON.stringify(APIResponse));
            console.log(xhr.responseText);
        }
        }
        xhr.open("POST", "http://ec2-44-238-219-22.us-west-2.compute.amazonaws.com:8086/offerFeedback");
        xhr.setRequestHeader("Content-Type", "application/json")
        xhr.send(json);
    }
}

customElements.define('offer-main', OfferMain);