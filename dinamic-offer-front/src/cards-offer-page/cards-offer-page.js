import { LitElement, html, css } from 'lit-element';
class CardsOfferPage extends LitElement {
    
    static get styles() {
        return css`
            .imgClassOffer {
                width: 17.9rem;
                height: 19rem;
            }

            .titleOfferClass {
                font-size:12px;
                height: 6.74rem;
            }
        `;
    }

    static get properties() {
        return {
            nombreOffer: {
                type: String
            },
            tituloOffer: {
                type: String
            },
            descripcionOffer: {
                type: String
            },
            urlBanner: {
                type: String
            },
            landingUrl: {
                type: String
            }
        };
    }

    constructor() {
        super();
        this.urlBanner = "";
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div class="card h-100 text-center" style="width: 18rem;">
                <img class="card-img-top imgClassOffer" src="${this.urlBanner}" alt="Card image cap" >

                <ul class="list-group list-group-flush bg-info">
                    <li class="list-group-item bg-info font-weight-bold text-white titleOfferClass">
                        ${this.tituloOffer}
                        <br/>
                        <br/>
                        ${this.descripcionOffer}
                    </li>
                </ul>

                <div class="card-footer bg-info">
                    <a @click="${this.openLanding}" class="btn btn-light btn-sm" target="_blank" href="${this.landingUrl}" role="button">Ver más</a>
                </div>
            </div>
        `;
    }

    openLanding() {
        console.log("openLanding");
        this.dispatchEvent(new CustomEvent("open-landing",{}));
    }
}

customElements.define('cards-offer-page', CardsOfferPage);